package net.taller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.DelegatingServerHttpResponse;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosController {
    private ArrayList<Producto> listaProductos = null;
    public ProductosController(){
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1, "Portatil","Electronicos",550.45));
        listaProductos.add(new Producto(2, "Vajilla","Varios",65.25));
        listaProductos.add(new Producto(2, "Calculadora","Escolares",45.15));
    }

    /*Obtener Lista de Productos*/
    @GetMapping(value = "/productostaller", produces = "application/json")
    public ResponseEntity<List<Producto>> obtenerProductos()
    {
        System.out.println("Estoy en Obtener Productos");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    /*Obtener Producto Por Id*/
    @GetMapping(value = "productostaller/{id}")
    public ResponseEntity<Producto> getProductoId(@PathVariable int id)
    {
        Producto resultado = null;
        resultado = listaProductos.get(id);
        if (resultado == null) {
            return new ResponseEntity("Producto no Encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(resultado);
    }

    /* Agregar un nuevo producto */
    @PostMapping(value = "/productostaller", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody Producto nuevoProducto){
        listaProductos.add(nuevoProducto);
        return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }

    /*Actualizar Producto*/
    @PutMapping("/productostaller")
    public ResponseEntity updateProducto(@RequestBody Producto productoUpdate) {

        ResponseEntity<String> resultado = null;
        try {
            Producto productoAModificar = listaProductos.get(productoUpdate.getId());
            productoAModificar.setNombre(productoUpdate.getNombre());
            productoAModificar.setCategoria(productoUpdate.getCategoria());
            productoAModificar.setPrecio(productoUpdate.getPrecio());
            listaProductos.set(productoUpdate.getId(), productoAModificar);
            return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("Producto no Encontrado.", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /*Eliminar Producto*/
    @DeleteMapping("/productostaller/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id) {

        ResponseEntity<String> resultado = null;
        try
        {
            Producto productoAEliminar = listaProductos.get(id);
            listaProductos.remove(productoAEliminar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /*PATCH Producto Actualiar Precio*/
    @PatchMapping("/productostaller/{id}")
    public ResponseEntity patchPrecioProducto(@RequestBody Producto productoPatch, @PathVariable int id) {

        ResponseEntity<String> resultado = null;
        try {
            Producto productoModificado = listaProductos.get(id);
            productoModificado.setPrecio(productoPatch.getPrecio());

            return new ResponseEntity<>("Precio actualizado correctamente.", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("Producto no Encontrado.", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /*Obtener Producto Por id y Nombre*/
    @GetMapping(value = "productostaller/{id}/nombre")
    public ResponseEntity<Producto> getProductoIdNombre(@PathVariable int id)
    {
        Producto resultado = null;
        resultado = listaProductos.get(id);
        if (resultado == null) {
            return new ResponseEntity("Producto no Encontrado.", HttpStatus.NOT_FOUND);
        }
        if (resultado.getNombre() != null)
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        else
            return new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
    }
}
